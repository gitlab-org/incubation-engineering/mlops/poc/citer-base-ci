import os

if __name__ == '__main__':
    variable_names = os.environ.get('CITER_VARIABLE_NAMES', "").split(",")

    definitions = []

    if len(variable_names) > 0:
        values = {v: os.environ.get(v) for v in variable_names}

        definitions += [
            "",
            "variables:",
            *[f"    {name}: {value}" for name, value in values.items()],
            ""
        ]

    extra_files_url = os.environ.get('CITER_EXTRA_FILES_URL', None)

    if extra_files_url:
        definitions += [
            '',
            'before_script:',
            f"    - curl {extra_files_url} | tar -zxv",
            ''
        ]

    if definitions:
        print("\n".join(['', '# ADDED BY CITER', *definitions]))






